/**
 * Created by instancetype on 6/15/14.
 */
var Photo = require('../models/Photo')
  , path = require('path')
  , join = path.join
  , fs = require('fs')
  , formidable = require('formidable')
  , util = require('util')

var express = require('express')
  , router = express.Router()

module.exports = function(photoDir) {

  router.get('/', function(req, res, next) {
    Photo.find({}, function(err, photos) {
      if (err) return next(err)

      res.render('photos', { title : 'Express 4 PhotoShare'
                           , photos : photos
                           })
    })
  })

  router.get('/upload', function(req, res, next) {
    res.render('photos/upload', { title : 'Photo Upload' });
  });

  router.post('/upload', function(req, res, next) {
    var form = new formidable.IncomingForm()

    form.parse(req, function(err, fields, files) {
      var image = files.photoImage.name
        , name = fields.photoName || image
        , desiredPath = join(photoDir, image)

      fs.rename(files.photoImage.path, desiredPath, function(err) {
        if (err) return next(err)

        Photo.create({
                       name : name
                     , path : image
                     }
                     , function(err) {
                         if (err) {
                         return next(err)
                     }
                     res.redirect('/')
                     })
      })
    })
  })

  router.get('/:id/download', function(req, res, next) {
    var id = req.params.id

    Photo.findById(id, function(err, photo) {
      if (err) return next(err)

      var path = join(photoDir, photo.path)
      res.download(path, photo.path)
    })
  })

  return router
}